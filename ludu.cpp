#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

class Square
{
    int position_no;
    int special_move;
    int move_to_position;
public:
    Square(){}

    Square(int s, int p, int m)
    {
        this->special_move = s;
        this->position_no = p;
        this->move_to_position = m;
    }

    int getPosition() { return this->position_no; }
    int getSpecialMove() { return this->special_move; }
    int getMoveToPosition() { return this->move_to_position; }
};

class Board
{
    Square board[48];
public:
    Board()
    {
        for(int i = 0; i < 48; i++){
            if(i == 4) board[i] = Square(2, i, 17);
            else if(i == 6) board[i] = Square(2, i, 20);
            else if(i == 10) board[i] = Square(2, i, 23);
            else if(i == 12) board[i] = Square(1, i, 5);
            else if(i == 13) board[i] = Square(2, i, 27);
            else if(i == 15) board[i] = Square(1, i, 7);
            else if(i == 18) board[i] = Square(2, i, 24);
            else if(i == 21) board[i] = Square(1, i, 7);
            else if(i == 25) board[i] = Square(1, i, 14);
            else if(i == 26) board[i] = Square(2, i, 41);
            else if(i == 28) board[i] = Square(1, i, 16);
            else if(i == 32) board[i] = Square(1, i, 13);
            else if(i == 34) board[i] = Square(1, i, 22);
            else if(i == 35) board[i] = Square(2, i, 38);
            else if(i == 45) board[i] = Square(1, i, 9);
            else board[i] = Square(0, i, i);
        }
    }

    Square squareInfo(int i) { return this->board[i]; }

    void display()
    {
        cout << "----------------------------------------------------------\n";
        for(int i = 47; i >= 42; i--){
            if(board[i].getSpecialMove() == 1){
                cout <<" " << i << "(S," << board[i].getMoveToPosition() << ")  ";
            }else if(board[i].getSpecialMove() == 2){
                cout <<" " << i << "(L," << board[i].getMoveToPosition() << ")  ";
            }else{
                cout <<" " << i << "       ";
            }
        }
        cout << "\n----------------------------------------------------------\n";

        for(int i = 36; i <= 41; i++){
            if(board[i].getSpecialMove() == 1){
                cout <<" " << i << "(S," << board[i].getMoveToPosition() << ") ";
            }else if(board[i].getSpecialMove() == 2){
                cout <<" " << i << "(L," << board[i].getMoveToPosition() << ") ";
            }else{
                cout <<" " << i << "       ";
            }
        }
        cout << "\n----------------------------------------------------------\n";

        for(int i = 35; i >= 30; i--){
            if(board[i].getSpecialMove() == 1){
                cout <<" " << i << "(S," << board[i].getMoveToPosition() << ") ";
            }else if(board[i].getSpecialMove() == 2){
                cout <<" " << i << "(L," << board[i].getMoveToPosition() << ") ";
            }else{
                cout <<" " << i << "       ";
            }
        }
        cout << "\n----------------------------------------------------------\n";

        for(int i = 29; i >= 24; i--){
            if(board[i].getSpecialMove() == 1){
                cout <<" " << i << "(S," << board[i].getMoveToPosition() << ") ";
            }else if(board[i].getSpecialMove() == 2){
                cout <<" " << i << "(L," << board[i].getMoveToPosition() << ") ";
            }else{
                cout <<" " << i << "       ";
            }
        }
        cout << "\n----------------------------------------------------------\n";

        for(int i = 23; i >= 18; i--){
            if(board[i].getSpecialMove() == 1){
                cout <<" " << i << "(S," << board[i].getMoveToPosition() << ")  ";
            }else if(board[i].getSpecialMove() == 2){
                cout <<" " << i << "(L," << board[i].getMoveToPosition() << ")  ";
            }else{
                cout <<" " << i << "       ";
            }
        }
        cout << "\n----------------------------------------------------------\n";

        for(int i = 12; i <= 17; i++){
            if(board[i].getSpecialMove() == 1){
                cout <<" " << i << "(S," << board[i].getMoveToPosition() << ")  ";
            }else if(board[i].getSpecialMove() == 2){
                cout <<" " << i << "(L," << board[i].getMoveToPosition() << ")  ";
            }else{
                cout << i << "       ";
            }
        }
        cout << "\n----------------------------------------------------------\n";

        for(int i = 11; i >= 6; i--){
            if(board[i].getSpecialMove() == 1){
                cout <<" " << i << "(S," << board[i].getMoveToPosition() << ") ";
            }else if(board[i].getSpecialMove() == 2){
                cout <<" " << i << "(L," << board[i].getMoveToPosition() << ") ";
            }else{
                cout <<" " << i << "       ";
            }
        }
        cout << "\n----------------------------------------------------------\n";

        for(int i = 0; i <= 5; i++){
            if(i == 0) cout << " Start     ";
            else if(board[i].getSpecialMove() == 1){
                cout <<" " << i << "(S," << board[i].getMoveToPosition() << ") ";
            }else if(board[i].getSpecialMove() == 2){
                cout <<" " << i << "(L," << board[i].getMoveToPosition() << ") ";
            }else{
                cout <<" " << i << "       ";
            }
        }
        cout << "\n----------------------------------------------------------\n";
    }
};

class Player
{
    string name;
    int current_position;
    int total_number_of_moves;

public:
    Player(){}

    Player(string name, int pos, int moves)
    {
        this->name = name;
        this->current_position = pos;
        this->total_number_of_moves = moves;
    }

    void setName(string name) { this->name = name; }
    void setCurrentPosition(int pos) { this->current_position = pos; }
    void setTotalMoves(int moves) { this->total_number_of_moves = moves; }

    string getName() { return this->name; }
    int getCurrentPosition() { return this->current_position; }
    int getTotalMoves() { return this->total_number_of_moves; }
};

class GamePlay
{
    Board playBoard;

public:
    bool play(Player& player)
    {
        int playerMove;

        while(true){
            playerMove = player.getCurrentPosition() + ((rand()%3) + 1);

            if(playerMove <= 47) break;
        }

        playerMove = playBoard.squareInfo(playerMove).getMoveToPosition();
        player.setCurrentPosition(playerMove);
        player.setTotalMoves(player.getTotalMoves() + 1);

        this->playBoard.display();

        if(playerMove == 47) return true;

        return false;
    }
};

int main()
{
        srand(time(0));
        bool firstMove = true;
        bool game_over = false;
        Player first("A", 0, 0);
        Player second("B", 0, 0);
        GamePlay game;

        while(!game_over){
            char input;
            if(firstMove){
                cout << "First player enter an input: ";
                cin >> input;

                while(input != 'R'){
                    cout << "First player enter an input: ";
                    cin >> input;
                }

                bool res = game.play(first);
                if(res) game_over = true;
                cout << "Player " << first.getName() << " moved to position " << first.getCurrentPosition() << endl;
            }else{
                cout << "Second player enter an input: ";
                cin >> input;

                while(input != 'R'){
                    cout << "Second player enter an input: ";
                    cin >> input;
                }

                bool res = game.play(second);
                if(res) game_over = true;
                cout << "Player " << second.getName() << " moved to position " << second.getCurrentPosition() << endl;
            }

            firstMove = !firstMove;
        }

        cout << "Game is over!!!" << endl;
}
